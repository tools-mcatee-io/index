# index

## design thoughts

1. Serve most tools through the main index page
2. Any tools that need large dependencies should be served as different pages from this same project
3. Any tools of scale should go in a different project (with a distinct [custom domain](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/index.html) assigned, following the pattern `*.tools.mcatee.io`)